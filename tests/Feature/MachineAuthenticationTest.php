<?php
namespace ImpactFactoring\Accounts\Tests\Feature;

use ImpactFactoring\Accounts\Client\ImpactFactoringAccountsMachineClient;
use ImpactFactoring\Accounts\Tests\TestCase;

class MachineAuthenticationTest extends TestCase
{
    public function test_machine_client_can_authenticate()
    {
        $client = new ImpactFactoringAccountsMachineClient();

        self::assertTrue($client->isAuthenticated());
    }

    public function test_machine_client_can_fetch_a_user_by_id()
    {
        $client = new ImpactFactoringAccountsMachineClient();

        $response = $client->getUserById('1');

        $this->assertArrayHasKey('id', $response);
        $this->assertArrayHasKey('first_name', $response);
        $this->assertArrayHasKey('last_name', $response);
        $this->assertArrayHasKey('email', $response);
        $this->assertArrayHasKey('type', $response);


        //self::assertFalse($client->isAuthenticated());
    }
}