# Impact Factoring Accounts Client

## Installation

```bash
composer require impact-factoring/accounts-client
```

## Methods

### `__construct(string $baseUrl)`

Constructor method to initialize the client with the base URL.

- **Parameters:**
    - `$baseUrl` (string): The base URL for the API.

---

### `login(array $credentials): ?array`

Logs a user into the system using provided credentials.

- **Parameters:**
    - `$credentials` (array): An array containing user credentials.

- **Returns:**
    - `null|array`: Returns user data if login is successful, `null` otherwise.

---

### `logout(?string $token = null): ?bool`

Logs out a user from the system.

- **Parameters:**
    - `$token` (string|null): Optional. The authentication token of the user.

- **Returns:**
    - `null|bool`: Returns `true` if logout is successful, `false` otherwise.

---

### `authenticate(?string $token): bool`

Authenticates a user token.

- **Parameters:**
    - `$token` (string|null): Optional. The authentication token of the user.

- **Returns:**
    - `bool`: Returns `true` if authentication is successful, `false` otherwise.

---

### `getUserByToken(?int $userId = null, ?string $token = null): ?array`

Fetches user data using provided user ID and token.

- **Parameters:**
    - `$userId` (int|null): Optional. The ID of the user.
    - `$token` (string|null): Optional. The authentication token of the user.

- **Returns:**
    - `null|array`: Returns user data if retrieval is successful, `null` otherwise.
