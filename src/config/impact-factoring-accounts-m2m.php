<?php

return [
    'base_url'      => env('IMPACT_FACTORING_ACCOUNTS_BASE_URL'),
    'client_id'     => env('IMPACT_FACTORING_ACCOUNTS_CLIENT_ID'),
    'client_secret' => env('IMPACT_FACTORING_ACCOUNTS_CLIENT_SECRET'),
];