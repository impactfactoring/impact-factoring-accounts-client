<?php

namespace ImpactFactoring\Accounts\Providers;

use Illuminate\Support\ServiceProvider;

class AccountsM2MServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $configPath = realpath(__DIR__ . '/../../config/impact-factoring-accounts-m2m.php');

        $this->mergeConfigFrom($configPath, 'impact-factoring-accounts-m2m');
    }
}