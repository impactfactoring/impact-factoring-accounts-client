<?php

namespace ImpactFactoring\Accounts\Client;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use ImpactFactoring\Accounts\Traits\SendsRequests;

class ImpactFactoringAccountsMachineClient
{
    use SendsRequests;

    protected string $clientId;
    protected string $clientSecret;
    protected ?string $accessToken = null;

    /**
     * @throws \Illuminate\Http\Client\ConnectionException
     */
    public function __construct()
    {
        $this->baseUrl = Config::get('impact-factoring-accounts-m2m.base_url');
        $this->clientId = Config::get('impact-factoring-accounts-m2m.client_id');
        $this->clientSecret = Config::get('impact-factoring-accounts-m2m.client_secret');

        $this->authenticate($this->clientId, $this->clientSecret);
    }

    /**
     * @throws \Illuminate\Http\Client\ConnectionException
     */
    public function getUserById(string $userId): ?array
    {
        if (!$this->isAuthenticated()) {
            $this->authenticate($this->clientId, $this->clientSecret);
        }

        $response = $this->sendRequest("/m2m/user/$userId", 'GET', $this->accessToken);

        if ($response->failed()) {
            return null;
        }

        return $response->json();
    }

    /**
     * @throws \Illuminate\Http\Client\ConnectionException
     */
    private function authenticate(string $clientId, string $clientSecret): void
    {
         $response = Http::withHeaders(['Accept' => 'application/json'])
             ->post($this->baseUrl . '/oauth/token', [
                'grant_type' => 'client_credentials',
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'scope' => 'accounts_machine_client',
            ]);

         if ($response->ok()) {
             $this->accessToken = $response->json()['access_token'];
         }
    }

    public function isAuthenticated(): bool
    {
        return !!$this->accessToken;
    }
}