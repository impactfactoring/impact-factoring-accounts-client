<?php

namespace ImpactFactoring\Accounts\Client;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Log;
use ImpactFactoring\Accounts\Traits\SendsRequests;

class ImpactFactoringAccountsClient
{
    use SendsRequests;

    protected string $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public function login(array $credentials): ?array
    {
        try {
            $response = $this->sendRequest('/login', 'post', data: $credentials);
            if ($response->successful()) {
                $userData = $response->json();
                $userData['auth_token'] = $this->getTokenFromResponse($response->cookies()->toArray());

                return $userData;
            }
        } catch (ConnectionException $e) {
            Log::error($e->getMessage());
        }

        return null;
    }

    public function logout(?string $token = null): ?bool
    {
        try {
            return $this->sendRequest('/logout', 'post', token: $token)->successful();
        } catch (ConnectionException $e) {
            Log::error($e->getMessage());
        }

        return null;
    }

    public function authenticate(?string $token): bool
    {
        if (!$token) {
            return false;
        }

        try {
            return ($this->sendRequest('/authenticate', token: $token))->successful();
        } catch (ConnectionException $e) {
            Log::error($e->getMessage());
        }

        return false;
    }

    public function getUserByToken(?int $userId = null, ?string $token = null): ?array
    {
        if (!$token) {
            return null;
        }

        //        if ($user = Cache::get($userId)) {
        //            return $user;
        //        }

        $endpoint  = $userId ? "/user/$userId" : '/authenticate';

        try {
            $response = $this->sendRequest($endpoint, 'get', $token);

            if ($response->successful()) {
                $user = array_merge($response->json(), ['auth_token' => $token]);

                //                Cache::put($userId, $user, 300);

                return $user;
            }
        } catch (ConnectionException $e) {
            Log::error($e->getMessage());
        }

        return null;
    }

    private function getTokenFromResponse(array $cookies): string
    {
        if (!isset($cookies[0])) {
            return false;
        }

        return urldecode($cookies[0]['Value']);
    }
}
