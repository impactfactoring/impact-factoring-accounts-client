<?php

namespace ImpactFactoring\Accounts\Traits;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

trait SendsRequests
{
    protected string $baseUrl = '';

    /**
     * @throws ConnectionException
     */
    protected function sendRequest(string $endpoint, string $method = 'get', ?string $token = null, array $data = []): Response
    {
        $headers = [
            "Accept" => "application/json",
        ];

        if ($token) {
            $headers['Authorization'] = "Bearer $token";
        }

        $endpoint = ltrim($endpoint, '/');
        $url = "$this->baseUrl/api/v1/$endpoint";

        return Http::withHeaders($headers)->{strtolower($method)}($url, $data);
    }
}